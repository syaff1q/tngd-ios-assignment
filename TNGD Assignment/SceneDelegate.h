//
//  SceneDelegate.h
//  TNGD Assignment
//
//  Created by Muhammad Nur Syafiq Sinwan on 13/01/2022.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

