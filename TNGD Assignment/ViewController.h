//
//  ViewController.h
//  TNGD Assignment
//
//  Created by Muhammad Nur Syafiq Sinwan on 13/01/2022.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *informationLabel;
@property (weak, nonatomic) IBOutlet UIButton *toogleButton;

@end

