//
//  ViewController.m
//  TNGD Assignment
//
//  Created by Muhammad Nur Syafiq Sinwan on 13/01/2022.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()

@end

@implementation ViewController

BOOL isBackgroundRedColor = NO;

NSTimer *timer;
int totalSeconds;

NSString * string1 = @"Changing view color from";;
NSString * string2;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //give some design to button
    [self buttonDesign];
    
    //set button to initial state
    [self initialCondition];
    
}


#pragma mark Button Action
- (IBAction)toogleButtonPressed:(id)sender {
    
    //show confirmation alert. for a sensitive action, it is necessary to make a confirmation with end user
    [self changingViewColorAlertConfirmation];
    
}

#pragma mark Countdown Timer
-(void)startTimer {
    
    //your time
    totalSeconds = 4;
    
    //set timer interval
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
             
    //define which function to call when the timer start
    selector:@selector(updateCountDownTime) userInfo:nil repeats:YES];
 }

-(void)updateCountDownTime {
    [self populateLabelwithTime:totalSeconds];
        
        if( totalSeconds != 0) {
            //deduct 1 seconds if current seconds is not 0
            totalSeconds -= 1;
        } else {
            
            //after 5 seconds, check current color and switch them
            if (isBackgroundRedColor) {
                self.view.backgroundColor = [UIColor greenColor];
                isBackgroundRedColor = NO;
            }
            else {
                self.view.backgroundColor = [UIColor redColor];
                isBackgroundRedColor = YES;
            }
            
            //stop timer
            [timer invalidate];
            
            //reset to initial state
            [self initialCondition];
            
         }
}

//information label on how many seconds left before color change
- (void)populateLabelwithTime:(int)seconds {
    self.informationLabel.text = [NSString stringWithFormat:@"%@\n%@ in %i", string1, string2, totalSeconds];
}

#pragma mark Helper
- (void)initialCondition {
    
    //hide information label
    [self.informationLabel setHidden:YES];
    
    if (isBackgroundRedColor) {
        string2 = @"red to green";
        
        //preset information text to display later
        self.informationLabel.text = [NSString stringWithFormat:@"%@\n%@ in 5", string1, string2];
    }
    else {
        string2 = @"green to red";
        
        //preset information text to display later
        self.informationLabel.text = [NSString stringWithFormat:@"%@\n%@ in 5", string1, string2];
    }
    
    //set button background color to blue
    self.toogleButton.backgroundColor = [UIColor blueColor];
    
    //enable butotn interaction
    self.toogleButton.enabled = YES;
}

- (void)buttonDesign {
    
    //set corner radius and cliptobounds
    self.toogleButton.layer.cornerRadius = 10;
    self.toogleButton.clipsToBounds = YES;
    
    //set button name, easier for future localization
    [self.toogleButton setTitle:@"Click Me!" forState:UIControlStateNormal];
}

#pragma mark Confirmation to Change color
- (void)changingViewColorAlertConfirmation {
    
    //Setup alert title and message
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Change View Color" message:@"You are about to change this view color to a new color. Proceed?" preferredStyle:UIAlertControllerStyleAlert];
    
    //Okay button
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //disable button so that user cannot click it multiple times during an action
        self.toogleButton.enabled = NO;
        
        //using alpha to indicate that the button is clicked
        self.toogleButton.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent:0.5f];
        
        //unhide the information label
        [self.informationLabel setHidden:NO];
        
        //start the timer
        [self startTimer];
    }];
    
    //Cancel button
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:cancel];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
